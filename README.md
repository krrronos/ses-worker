# SES Worker

## Architecture

## Terminology

`backend` - mailgun/sendrid/etc: a service for sending emails

`inactive backend` - backend which was turned off for some reason

`worker` - a working instance of this project  

`FBC` - Failed Backend Checker. Background job which is checking backends every minute. 

`Redis state` - shared state about backend statuses

`internal state` - worker's internal state about available backends

## Initialization

1. Worker connects to redis & rabbitmq, subscribes to `emails` queue in rabbitmq and redis' 
`ses:backend-up` and `ses:backend-down` events. 
2. Initialize FBC. It will be fired every minute.
3. Checks for `ses:alive-email-backends` key in redis and depends on its existance syncs available
backends to internal state or initialize redis with all available backends.

## Handle emails

After each event from rabbitmq worker selectes next backend from its internal state and uses it to
send email. If there is no available backends an exception will be raised and email will stay in queue.
If during email sending happens an exception, the app marks current backend as bad (see Backend's fall) and
try to send email with next backend. 

## Backend's rebirth

1. Add it to redis state
2. Notify all workers that backend is up. Upon notification workers add the backend
to its internal state.

## Backend's fall

1. Remove backend from redis state.
2. Acquire lock for modify backend's state in redis.
3. Notify all workers (including yourself) that a backend is down.
Upon notification workers remove the backend from its internal state.
4. Save information about fall (time and stage) in backend redis state (see Redis state below).
If there is already some stage then set failed_at key to current time and increase stage.
Stages are made in order to detect long backend falls:
For example on first fail backend will be marked as down for a 1 minute (stage #0) 
but if an error with this backend happen again during first 5 minites (stage #1) then
backend will be turned off for a 5 minutes (stage #2) and will be monitored for 30 minutes
(stage #3). Time sheets were taken from my head.

## Failed Backend Checker

1. Take difference between all available backends and active (all inactive backend)
2. Iterate over the result: if there is no information about inactive backend in redis state
 then backend should be marked as active.
3. If backend failed more than stage's amount of time ago and marked it as active

## Usage

1. Install [leiningen](https://leiningen.org/)
2. Install [PostgreSQL](https://www.postgresql.org/) 9.1 or higher, 
3. Perform [migrations](https://gitlab.com/krrronos/emailer/tree/master/migrations)
4. Install [redis](https://redis.io/) at least 3.2.
5. Install [RabbitMQ](https://www.rabbitmq.com/) at least 3.7.6.
6. Run `cp .lein-env-example .lein-env` and edit all variables
7. Run `lein run`

## Redis state

| Key       | Data type   | Description   | Example  |
|-----------|------------|---------------|----------|
|`ses:alive-email-backends` | set | List of available backends   | `["mailgun", "sendgrid"]`   |
|`ses:email-backend-:<backend>` | string | Keeps data when and on which stage backend had failed | `{"stage": 0, "failed_at": "2015-08-09T18:31:42Z"}` |
