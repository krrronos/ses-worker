(ns me.kronos.ses.worker.backends.mailgun
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.worker.email :as email]
            [clojure.tools.logging :as log]
            [mailgun.mail :as mail]))

(defn- ->mailgun
  [{:keys [message] :as mail}]
  (select-keys mail [:to :from :subject :text]))

(defn- send*
  [{:keys [api-key domain]} mail]
  (let [creds {:key api-key :domain domain}]
    (try
      (let [answer (->> (->mailgun mail)
                        (mail/send-mail creds))]
        (= 200 (:status answer)))
      (catch Exception e
        (log/error e)
        false))))

(defrecord Mailgun []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (assoc this :api-key (env :mailgun-api-key) :domain (env :mailgun-domain) :name :mailgun))
  (stop [this]
    (assoc this :api-key nil :domain nil :name nil))
  email/Email
  (send [this email]
    (send* this email)))

(defn create
  []
  (->Mailgun))
