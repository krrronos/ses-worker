(ns me.kronos.ses.worker.failover.simple
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.worker.cache :as cache]
            [me.kronos.ses.worker.failover :as failover]
            [clj-time.core :as time]
            [taoensso.carmine :as car]
            [me.kronos.ses.worker.cache.redis :as redis]
            [clojure.set :as set]
            [clojure.tools.logging :as log]
            [twarc.core :as twarc]
            [clj-time.format :as time-format]))

(def ^:private email-backends-key
  "ses:alive-email-backends")

(def ^:private backend-up-msg
  "ses:backend-up")

(def ^:private backend-down-msg
  "ses:backend-down")

(def ^:private check-interval
  (* 60 1000)) ; 1 minute

(def ^:private failing-stages
  ; joda-time -> milliseconds
  (map #(-> % .toPeriod .toStandardSeconds .getSeconds)
       [(time/minutes 1)
        (time/minutes 5)
        (time/minutes 30)
        (time/hours   1)
        (time/days    1)
        (time/weeks   1)
        (time/days    30)
        (time/days    356)
        (time/days    (* 50 365))
        (time/days    (* 50 365))])) ; "indefinetly"

(defn- backend-key
  [backend]
  (str "ses:email-backend-" backend))

(defn next-stage
  ([]
   (next-stage -1))
  ([stage]
   {:stage (inc stage) :failed_at (time/now)}))

(defn- check-disabled-backends
  [_ {:keys [backends alive-backends redis] :as <failover>}]
  (doseq [backend (set/difference (set (keys backends)) (set @alive-backends))]
    (let [{:keys [stage failed_at]} (some-> (backend-key backend)
                                            (->> (cache/lookup-json redis))
                                            (update :failed_at #(time-format/parse %)))]
      (when (or (nil? failed_at) (time/after? (time/now) failed_at))
        (failover/backend-up <failover> backend)))))

(defrecord SimpleFailover []
  component/Lifecycle
  (start [{:keys [redis mailgun sendgrid fallen-backends-sched] :as this}]
    (let [backends       {:mailgun mailgun :sendgrid sendgrid}
          alive-backends (atom [])
          this*          (assoc this :backends       backends
                                     :alive-backends alive-backends
                                     :counter        (atom 0))]

      (cache/subscribe
        redis
        backend-up-msg
        (fn [[msg _ backend]]
          (when (= msg "message")
            (log/info "backend up:" backend)
            (swap! alive-backends conj (keyword backend)))))

      (cache/subscribe
        redis
        backend-down-msg
        (fn [[msg _ backend]]
          (when (= msg "message")
            (log/info "backend down:" backend)
            (swap! alive-backends #(remove (fn [e] (= e (keyword backend))) %)))))

      (twarc/schedule-job fallen-backends-sched
                          #'check-disabled-backends [this*]
                          :job {:identity "fallen-backends-sched"}
                          :trigger {:simple   {:repeat   :inf
                                               :interval check-interval}
                                    :start-at (.toDate (time/plus (time/now) (time/millis check-interval)))}
                          :replace true)

      ; check if key exists in redis
      (if (= 1 (cache/cmd redis car/exists email-backends-key))
        (->> (cache/cmd redis car/smembers email-backends-key)
             (mapv keyword)
             (reset! alive-backends))
        (doseq [backend (keys backends)]
          (failover/backend-up this* backend)))

      this*))
  (stop [{:keys [fallen-backends-sched] :as this}]
    (twarc/delete-job fallen-backends-sched "fallen-backends-sched")
    (assoc this :backends nil :alive-backends nil :counter nil))

  failover/Failover
  (backend-up [{:keys [redis alive-backends]} backend]
    (cache/cmd redis car/sadd [email-backends-key backend])
    (cache/publish redis backend-up-msg backend))

  (backend-down [{:keys [redis alive-backends]} backend]
    (cache/cmd redis car/srem [email-backends-key backend])
    (let [key (backend-key backend)]
      (cache/within-lock redis (str key "-lock") 15000 10
        (fn []
          (cache/publish redis backend-down-msg backend)
          (if-let [{:keys [stage]} (cache/lookup-json redis key)]
            (cache/cache-json redis key (next-stage stage) (nth failing-stages (+ 2 stage)))
            (cache/cache-json redis key (next-stage) (second failing-stages)))))))

  (select-backend [{:keys [backends alive-backends counter]}]
    (let [candidates @alive-backends]
      (when-not (empty? candidates)
        (->> (swap! counter #(-> % inc (mod (count candidates))))
             (nth candidates)
             (keyword)
             (get backends))))))

(defn create []
  (->SimpleFailover))
