(ns me.kronos.ses.worker.queue)

(defprotocol Queue
  (subscribe [this queue-name f]))
