(ns me.kronos.ses.worker.system
  (:require [com.stuartsierra.component :as component]
            [me.kronos.ses.worker.app :as app]
            [me.kronos.ses.worker.backends.sendgrid :as sendgrid]
            [me.kronos.ses.worker.backends.mailgun :as mailgun]
            [me.kronos.ses.worker.failover.simple :as simple-failover]
            [me.kronos.ses.worker.cache.redis :as redis]
            [me.kronos.ses.worker.queue.rabbitmq :as rabbitmq]
            [me.kronos.ses.worker.scheduler :as scheduler]
            [twarc.core :as twarc]))

(defn ->system
  [env death-channel]
  (component/system-using
    (component/system-map
      :env                   env
      :app                   (app/create death-channel)
      :sendgrid              (sendgrid/create)
      :mailgun               (mailgun/create)
      :failover              (simple-failover/create)
      :redis                 (redis/create)
      :queue                 (rabbitmq/create)
      :fallen-backends-sched (twarc/make-scheduler scheduler/scheduler-properties
                                                  {:name "fallen-backends-sched"}))
    {:sendgrid              [:env]
     :mailgun               [:env]
     :redis                 [:env]
     :queue                 [:env]
     :failover              [:redis :sendgrid :mailgun :fallen-backends-sched]
     :app                   [:failover :queue]}))
