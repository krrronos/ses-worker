(ns me.kronos.ses.worker.cache.redis
  (:require [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [taoensso.carmine.locks :as locks]
            [taoensso.carmine :as car :refer (wcar)]
            [taoensso.carmine.connections :as conn]
            [me.kronos.ses.worker.cache :as cache]
            [cheshire.core :as json]))

(defn within-lock*
  ([this lock-name f]
   (within-lock* this lock-name 15000 500 f))
  ([{:keys [conn]} lock-name lock-time-ms lock-wait-time-ms f]
   (log/debug "trying to lock" lock-name)
   (locks/with-lock conn lock-name lock-time-ms lock-wait-time-ms
                    (log/debug "successfully locked" lock-name)
                    (let [res (f)]
                      (log/debug "releasing lock" lock-name)
                      res))))

(defn set*
  [{:keys [conn]} key val ttl-sec serialize-fn]
  (log/debug "Setting value for" key "with expiration time" ttl-sec)
  (let [val (serialize-fn val)]
    (car/wcar conn (car/set key val :ex ttl-sec))))

(defn lookup*
  [{:keys [conn]} key deserialize-fn]
  (log/debug "Looking in redis for" key)
  (->> (car/get key)
       (car/wcar conn)
       deserialize-fn))

(defn cmd*
  [{:keys [conn] :as <redis>} cmd args]
  (if (coll? args)
    (car/wcar conn (apply cmd args))
    (cmd* <redis> cmd [args])))

(defn subscribe*
  [{:keys [conn listeners]} key f]
  (car/with-new-pubsub-listener
    (:spec conn)
    {key f}
    (car/subscribe key)))

(defrecord Redis []
  component/Lifecycle
  (start [{:keys [env] :as this}]
    (try
      (let [conn {:pool (conn/conn-pool :mem/fresh {})
                  :spec {:uri (env :redis-url)}}]
        (car/wcar conn (car/ping))
        (assoc this :conn conn :listeners (atom [])))
      (catch Exception e
        (log/error e)
        this)))

  (stop [{:keys [listeners] :as this}]
    (when-let [conn-pool (get-in this [:conn :pool])]
      (.close conn-pool))
    (doseq [listener @listeners]
      (car/close-listener listener))
    (assoc this :conn nil))

  cache/Cache
  (within-lock [redis lock-name f]
    (:result (within-lock* redis lock-name f)))
  (within-lock [redis lock-name f lock-time-ms lock-wait-time-ms]
    (:result (within-lock* redis lock-name f lock-time-ms lock-wait-time-ms)))
  (cache [redis key value ttl-sec]
    (set* redis key value ttl-sec car/freeze))
  (cache-json [redis key value ttl-sec]
    (set* redis key value ttl-sec json/encode))
  (lookup [redis key]
    (lookup* redis key identity))
  (lookup-json [redis key]
    (lookup* redis key #(json/decode % true)))
  (subscribe [redis key f]
    (subscribe* redis key f))
  (publish [redis key msg]
    (car/wcar redis (car/publish key msg)))

  (cmd [redis cmd args]
    (cmd* redis cmd args)))


(defn create []
  (->Redis))
