(ns me.kronos.ses.worker.email
  (:refer-clojure :exclude [send]))

(defprotocol Email
  (send [this mail]))
