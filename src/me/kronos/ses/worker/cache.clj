(ns me.kronos.ses.worker.cache)

(defprotocol Cache
  (within-lock [this lock-name f] [this lock-name f lock-time-ms lock-wait-time-ms])
  (cache  [this key value ttl-sec])
  (cache-json [this key value ttl-sec])
  (lookup [this key])
  (lookup-json [this key])
  (cmd [this cmd args])
  (subscribe [this key f])
  (publish [this key msg]))
