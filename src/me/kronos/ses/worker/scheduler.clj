(ns me.kronos.ses.worker.scheduler
  (:require [plumbing.core :refer :all]))

(def scheduler-properties
  {:threadPool.threadCount    2
   :plugin.triggHistory.class "org.quartz.plugins.history.LoggingTriggerHistoryPlugin"
   :plugin.jobHistory.class   "org.quartz.plugins.history.LoggingJobHistoryPlugin"
   :threadPool.class          "org.quartz.simpl.SimpleThreadPool"})
