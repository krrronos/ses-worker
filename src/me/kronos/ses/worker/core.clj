(ns me.kronos.ses.worker.core
  (:require
    [clojure.tools.logging :as log]
    [me.kronos.ses.worker.system :as system]
    [com.stuartsierra.component :as component]
    [environ.core :refer [env]]
    [clojure.edn :as edn]
    [clojure.core.async :as a :refer [<!!]])
  (:import (org.slf4j.bridge SLF4JBridgeHandler)
           (org.joda.time DateTime))
  (:gen-class))

(extend-protocol cheshire.generate/JSONable
  DateTime
  (to-json [dt gen]
    (cheshire.generate/write-string gen (str dt))))

(defn init-logging! []
  (SLF4JBridgeHandler/removeHandlersForRootLogger)
  (SLF4JBridgeHandler/install)
  (Thread/setDefaultUncaughtExceptionHandler
    (reify Thread$UncaughtExceptionHandler
      (uncaughtException [_ thread ex]
        (log/error ex "Uncaught exception on" (.getName thread))))))

(defn -main [& args]
  (init-logging!)
  (let [death-channel (a/chan)
        sys (-> (system/->system env death-channel)
                (component/start))]
    (log/info "Worker has started")
    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. #(component/stop sys)))
    (<!! death-channel)
    (log/info "Worker has stopped")))
