(ns me.kronos.ses.worker.app
  (:require [com.stuartsierra.component :as component]
            [clojure.core.async :as a]
            [me.kronos.ses.worker.queue :as queue]
            [cheshire.core :as json]
            [me.kronos.ses.worker.failover :as failover]
            [me.kronos.ses.worker.email :as email]
            [clojure.tools.logging :as log]))

(defn message-handler
  [{<failover> :failover} channel metadata ^bytes payload]
  (let [{:keys [from_name to_name] :as mail} (-> (String. payload "UTF-8")
                                                 (json/parse-string true))
        mail* (cond-> mail
                from_name (-> (dissoc :from_name)
                              (update :from #(str from_name " <" % ">")))
                to_name   (-> (dissoc :to_name)
                              (update :to #(str to_name " <" % ">"))))]
    (loop [backend (failover/select-backend <failover>)]
      (if backend
        (when-not (email/send backend mail*)
          (failover/backend-down <failover> (:name backend))
          (recur (failover/select-backend <failover>)))
        (do
          (log/error "No available backends")
          (throw (Exception. "No available backends")))))))

(defrecord App [death-channel]
  component/Lifecycle
  (start [{:keys [queue] :as this}]
    (queue/subscribe queue "emails" (partial message-handler this))
    this)
  (stop [this]
    (a/close! death-channel)
    this))

(defn create
  [death-channel]
  (->App death-channel))
