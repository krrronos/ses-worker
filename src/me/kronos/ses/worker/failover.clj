(ns me.kronos.ses.worker.failover)

(defprotocol Failover
  (select-backend [this])
  (backend-up [this server])
  (backend-down [this server]))
