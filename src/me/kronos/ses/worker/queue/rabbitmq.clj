(ns me.kronos.ses.worker.queue.rabbitmq
  (:require [com.stuartsierra.component :as component]
            [clojure.tools.logging :as log]
            [langohr.core :as rmq]
            [langohr.channel :as lch]
            [langohr.queue :as lq]
            [langohr.basic :as lb]
            [langohr.consumers :as lc]
            [environ.core :refer [env]]
            [me.kronos.ses.worker.queue :as queue]))

(defn subscribe*
  [{:keys [ch]} queue-name f]
  (lc/subscribe ch queue-name (lc/ack-unless-exception f) {:auto-ack false}))

(defrecord Rabbitmq []
  component/Lifecycle
  (start [this]
    (let [conn (rmq/connect {:uri (env :cloudamqp-url)})
          ch   (lch/open conn)]
      (assoc this :ch ch :conn conn)))
  (stop [{:keys [conn ch] :as this}]
    (rmq/close ch)
    (rmq/close conn)
    (assoc this :ch nil :conn nil))

  queue/Queue
  (subscribe [this queue-name f]
    (subscribe* this queue-name f)))

(defn create
  []
  (->Rabbitmq))
