(ns user
  (:require
    [me.kronos.ses.worker.system :as system]
    [environ.core :refer [env]]
    [clojure.core.async :as a]
    [reloaded.repl :refer [system init start stop go reset reset-all]]
    [me.kronos.ses.worker.email :as email]))

(reloaded.repl/set-init! #(system/->system env (a/chan)))
