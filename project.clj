(defproject ses-worker "0.1.0"
  :description      "SES: worker"
  :url              "https://gitlab.com/krrronos/ses-worker"
  :license          {:name "Eclipse Public License"
                     :url "http://www.eclipse.org/legal/epl-v10.html"}
                     ; clojure
  :dependencies     [[org.clojure/clojure "1.9.0"]
                     [org.clojure/core.async "0.4.474"]
                     ; logging stuff
                     [ch.qos.logback/logback-classic "1.2.3"]
                     [org.slf4j/slf4j-api "1.7.25"]
                     [org.slf4j/jul-to-slf4j "1.7.25"]
                     [org.slf4j/jcl-over-slf4j "1.7.25"]
                     [org.slf4j/log4j-over-slf4j "1.7.25"]
                     [org.clojure/tools.logging "0.4.0"]
                     ; sentry
                     [com.getsentry.raven/raven-logback "7.7.0"]
                     [com.stuartsierra/component "0.3.2"]
                     ; utils
                     [prismatic/plumbing "0.5.5"]
                     ; environment management
                     [environ "1.1.0"]
                     ; rabbitmq
                     [com.novemberain/langohr "5.0.0"]
                     ; json
                     [cheshire "5.6.3"]
                     ; sendgrid
                     [clj-sendgrid "0.1.2"]
                     ; mailgun
                     [nilenso/mailgun "0.2.3"]
                     ; redis
                     [com.taoensso/carmine "2.18.0"]
                     ; background job
                     [twarc "0.1.10"
                      :exclusions [[org.clojure/clojure]
                                   [org.clojure/core.async]
                                   [com.stuartsierra/component]
                                   [prismatic/plumbing]]]]

  :resource-paths    ["config" "resources"]
  :profiles          {:dev     {:source-paths ["dev"]
                                :dependencies [[pjstadig/humane-test-output "0.8.3"]
                                               [reloaded.repl "0.2.4"]]
                                :repl-options {:init-ns user}
                                :resource-paths ["test/resources"]}
                       :uberjar {:aot :all}}
  :min-lein-version "2.0.0"
  :uberjar-name     "ses-worker-0.1.0-standalone.jar"
  :main             ^{:skip-aot true} me.kronos.ses.worker.core)
